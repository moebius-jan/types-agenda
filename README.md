# Installation
> `npm install --save @types/agenda`

# Summary
This package contains type definitions for Agenda (https://github.com/agenda/agenda).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/agenda

Additional Details
 * Last updated: Thu, 03 Oct 2019 22:49:37 GMT
 * Dependencies: @types/mongodb, @types/node
 * Global values: none

# Credits
These definitions were written by Meir Gottlieb <https://github.com/meirgottlieb>, and Jeff Principe <https://github.com/princjef>.
